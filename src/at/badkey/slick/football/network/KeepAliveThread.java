package at.badkey.slick.football.network;

import java.awt.Color;

import at.badkey.slick.football.FootballGame;

public class KeepAliveThread extends Thread{

	private GameSocket socket;
	
	public KeepAliveThread(GameSocket gameSocket) {
		this.socket = gameSocket;
	}

	@Override
	public void run() {
		while(true) {
			try {
				Thread.sleep(4000);
				socket.send("keepAlive");
				socket.sendGame("keepAlive");
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.out.println(FootballGame.timestamp + Color.RED + "KeepAliveThread shut down!");
			}
		}
	}
	
}
