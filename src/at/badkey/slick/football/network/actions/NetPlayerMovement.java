package at.badkey.slick.football.network.actions;

import at.badkey.slick.football.Character;
import at.badkey.slick.football.network.GameSocket;

public class NetPlayerMovement implements Runnable{
	
	private Character c;
	private GameSocket gameSocket;
	
	public NetPlayerMovement(Character c, GameSocket gameSocket) {
		this.c = c;
		this.gameSocket = gameSocket;
	}

	@Override
	public void run() {
		gameSocket.sendGame("posUpdate;"+c.getX()+";"+c.getY()+";"); //Command: posUpdate;X;Y; 0=> Command; 1=> X; 2=> Y; 3=> Clear buffer fields
		gameSocket.recieveGame();
		
	}
}
