package at.badkey.slick.football.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import at.badkey.slick.football.FootballGame;

public class GameSocket {
	public InetAddress SERVER_IP = null;
	public final int SERVER_PORT = 6500;
	public final int SERVER_GAMEPORT = 6501;
	
	private DatagramSocket socket;
	private DatagramSocket gameSocket;
	
	private int uid = -1;
	
	public GameSocket() {
		try {
			SERVER_IP = InetAddress.getByName("localhost");
			socket = new DatagramSocket();
			gameSocket = new DatagramSocket();
			
			System.out.println(FootballGame.timestamp + "Opened UDP sockets ("+socket.getLocalPort()+";"+gameSocket.getLocalPort()+").");
		} catch (Exception e) {
			System.out.println(FootballGame.timestamp+"Failed opening UDP socket");
			e.printStackTrace();
		}
		new KeepAliveThread(this).start();
	}
	
	public void init() {
		
	}
	
	public int getUId() {
		return uid;
	}
	
	public int getPort() {
		return socket.getLocalPort();
	}
	
	public int getGamePort() {
		return gameSocket.getLocalPort();
	}

	public void sendGame(String message) {
		//Connect to server
		try {
			byte[] data = message.getBytes();
			DatagramPacket packet = new DatagramPacket(data, data.length, SERVER_IP, SERVER_GAMEPORT);
			gameSocket.send(packet);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String recieveGame() {
		//Connect to server
		byte[] buffer = new byte[256];
		try {
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, SERVER_IP, SERVER_GAMEPORT);
			gameSocket.receive(packet);
			String resp = new String(packet.getData(), 0, packet.getLength());
			System.out.println("Recieved game message: " + resp);
			return resp;
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void send(String message) {
		//Connect to server
		try {
			byte[] data = message.getBytes();
			DatagramPacket packet = new DatagramPacket(data, data.length, SERVER_IP, SERVER_PORT);
			socket.send(packet);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String recieve() {
		//Connect to server
		byte[] buffer = new byte[256];
		try {
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, SERVER_IP, SERVER_PORT);
			socket.receive(packet);
			String resp = new String(packet.getData(), 0, packet.getLength());
			System.out.println("Recieved message: " + resp);
			return resp;
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	public String sendCommand(String command) {
		send(command);
		return recieve();
	}
}
