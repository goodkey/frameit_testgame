package at.badkey.slick.football;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public interface Character {
	
	public void draw(Graphics g);
	public void move(int delta, GameContainer gc);
	public int getHeight();
	public int getWidth();
	public int getX();
	public int getY();
	public void setX(int x);
	public void setY(int y);
	public Shape getHitbox();
	public void gravity(int delta);
}
