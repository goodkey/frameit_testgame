package at.badkey.slick.football.objects;

import org.newdawn.slick.Graphics;

import at.badkey.slick.football.FootballGame;

public class Ball {
	private int x, y, radius;
	private float accelerationY = 1;
	
	public int getRadius() {
		return this.radius;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public Ball(int x, int y, int radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public void update(int delta) {
		gravity(delta);
	}
	
	public void draw(Graphics g) {
		g.drawOval(this.x, this.y, radius, radius);
	}
	
	private long lastUpdate = System.currentTimeMillis();
	private void gravity(int delta) {
		if (getY() < FootballGame.HEIGHT - getRadius()) {
			setY((int) (getY() + SpeedControl.moveAmount(FootballGame.GRAVITY*accelerationY, delta)));
			if((System.currentTimeMillis() - lastUpdate) >= 200) {
				System.out.println("Hier");
				accelerationY = accelerationY + FootballGame.ACCELERATION;
				lastUpdate = System.currentTimeMillis();
			}
		}else if (accelerationY != 1){
			accelerationY = 1;
		}
	}
}
