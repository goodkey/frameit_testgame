package at.badkey.slick.football.objects;


import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import at.badkey.slick.football.Character;
import at.badkey.slick.football.FootballGame;

public class Homer implements Character {

	private Animation character;
	private Rectangle hitbox;
	private int x, y;
	private float curXSpeed, curYSpeed;
	public boolean isJumping = false;
	
	public final int caracterHeight = 300;
	public final int caracterWidth = 150;

	public Homer(int x, int y) {
		// Add the character to the canvis
		SpriteSheet sheet = null;
		try {
			sheet = new SpriteSheet("testdata/homeranim.png", 36, 65);
		} catch (SlickException e) {
			e.printStackTrace();
		}
		character = new Animation();
		for (int i = 0; i < 8; i++) {
			character.addFrame(sheet.getSprite(i, 0), caracterWidth);
		}

		hitbox = new Rectangle(x, y, caracterWidth, caracterHeight);

		this.x = x;
		this.y = y;
	}
	
	@Override
	public int getHeight() {
		return this.caracterHeight;
	}
	
	@Override
	public int getWidth() {
		return this.caracterWidth;
	}

	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public int getY() {
		return this.y;
	}
	
	@Override
	public void setX(int x) {
		this.x = x;
		hitbox.setX(x);
	}

	@Override
	public void setY(int y) {
		this.y = y;
		hitbox.setY(y);
	}
	
	public Shape getHitbox() {
		return hitbox;
	}
	
	@Override
	public void draw(Graphics g) {
		// Draw animation and hitbox
		character.draw(this.x, y, caracterWidth, caracterHeight);
		g.draw(hitbox);
	}

	@Override
	public void move(int delta, GameContainer gc) {
		boolean handled = false;
		
		jump(delta);	
		
		Input input = gc.getInput();
		if (input.isKeyDown(Input.KEY_D)) { //Move right
			character.start();
			double hip = x + SpeedControl.moveAmount(0.8, delta);
			this.curXSpeed = (float) SpeedControl.moveAmount(0.8, delta);

			this.x = (int) hip;
			hitbox.setX(this.x);
			if (hitbox.getX() >= FootballGame.WIDTH) {
				this.x = -caracterWidth;
			}
			handled = true;
		}
		if (input.isKeyDown(Input.KEY_A)) { //Move left
			character.start();
			double hip = x - SpeedControl.moveAmount(0.8, delta);
			this.curXSpeed = (float) SpeedControl.moveAmount(0.8, delta);

			this.x = (int) hip;
			hitbox.setX(this.x);
			if (hitbox.getX() >= FootballGame.WIDTH) {
				this.x = -149;
			}
			handled = true;
		}
		if (input.isKeyDown(Input.KEY_SPACE) && !this.isJumping) { //Move jump
			character.start();
			this.curYSpeed = 30f;
			this.isJumping = true;
			handled = true;
		}
		
		if(!handled) {
			character.stop();
		}
		
		
	}
	
	private void jump(int delta) {
		if(this.isJumping == true) {
			this.y = (int) (this.y - this.curYSpeed);
			this.hitbox.setY(this.y);
			this.curYSpeed --;
			if(this.curYSpeed <= 0) {
				this.isJumping = false;
			}
		}
		
	}
	public void gravity(int delta) {
		if (getY() < FootballGame.HEIGHT - getHeight()) {
			setY((int) (getY() + SpeedControl.moveAmount(FootballGame.GRAVITY, delta)));
		}

	}

}
