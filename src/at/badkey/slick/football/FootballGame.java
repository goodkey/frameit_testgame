package at.badkey.slick.football;

import java.sql.Timestamp;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import at.badkey.slick.football.network.GameSocket;
import at.badkey.slick.football.network.actions.NetPlayerMovement;
import at.badkey.slick.football.objects.Ball;
import at.badkey.slick.football.objects.Homer;

public class FootballGame extends BasicGame {

	private Image background;
	public static final int WIDTH = 1920;
	public static final int HEIGHT = 1080;
	
	public static String timestamp = "["+new Timestamp(System.currentTimeMillis())+"] ";

	public static final float GRAVITY = 0.8f;
	public static final float ACCELERATION = 1.001f;
	public float lastPosUpdate = System.currentTimeMillis();
	public static String UID = "-1";
	public static String GAMEID = "-1";
	public Character player = null;
	public Character netplayer = null;
	private GameSocket gameSocket = null;
	//private ArrayList<Shape> spawnedObjects = new ArrayList<Shape>();

	// Characters
	Character homer;
	Ball ball;

	public FootballGame() {
		super("JumpNRun");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer arg0, Graphics g) throws SlickException {
		background.draw(0, 0);
		// Draw character
		player.draw(g);
		netplayer.draw(g);
		ball.draw(g);
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		// Init the background
		background = new Image("jumpnrun/img/background_neu.jpg");

		// Init player
		homer = new Homer(10, 10);
		this.player = homer;
		
		//Init netplayer
		netplayer = new Homer(100, 10);
		
		//Init ball
		ball = new Ball(50, 50, 100);
		
		gameSocket = new GameSocket();
		
		UID = gameSocket.sendCommand("userid " + gameSocket.getGamePort());
		System.out.println("Recieved UID: "+UID);
		
		GAMEID = gameSocket.sendCommand("searchGame");
		System.out.println("Recieved GAMEID: "+GAMEID);
		
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// Check for character key inputs
		player.move(delta, gc);
		
		//Check for netplayer position
		if(System.currentTimeMillis() - lastPosUpdate >= 15.62) {
			new NetPlayerMovement(netplayer, gameSocket);
		}
		
		//Gravity
		player.gravity(delta);
		netplayer.gravity(delta);
		
		//Update other envoirmental things
		ball.update(delta);

	}


	public static void main(String[] args) throws SlickException {

		AppGameContainer jumpnrun = new AppGameContainer(new FootballGame());

		jumpnrun.setDisplayMode(WIDTH, HEIGHT, false);

		jumpnrun.setShowFPS(true);
		jumpnrun.setTargetFrameRate(60);
		jumpnrun.start();

	}

}
